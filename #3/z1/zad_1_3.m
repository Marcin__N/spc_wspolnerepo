clc;
clear all;
close all;
grid on;

s = tf('s');

%inercyjne

K=1;
T1=0.5;
T2=0.1;
Iner_1=tf(K,[T1 1]);
Iner_2=tf(K,[T1*T2 T1+T2 1]);


% oscylacyjny
wn=3;
ksi=0.3;
Kosc=1;
Osc=tf(Kosc,[1 2*ksi*wn wn^2]);

% calkujacy
ki=3;
Ti=2;
I=tf(ki,[1 0]);
IR=tf(ki,[Ti 1 0]);

% rozniczkujacy
kd=2;
Td=4;
D=tf([kd 0],1);
DR=tf([kd 0],[Td 1]);

%opoznienie
P = exp(-1*s);

num = 1;
den = [0 1];
P2 = tf(num,den,'InputDelay',1.0)


Models = [Iner_1, Iner_2, Osc, IR, DR, P, P2];
titles = {'Inercyjny_1_rzedu', 'Inercyjny_2_rzedu' ,'Oscylacyjny', 'calkujacy', 'rozniczkujacy', 'opozniajacy', 'op2'};
titles2 = {'Inercyjny_1_rzedu_Bode', 'Inercyjny_2_rzedu_Bode' ,'Oscylacyjny_Bode', 'calkujacy_Bode', 'rozniczkujacy_Bode', 'opozniajacy_Bode', 'op2_Bode'};
titles3 = {'Inercyjny_1_rzedu_Nyquist', 'Inercyjny_2_rzedu_Nyquist' ,'Oscylacyjny_Nyquist', 'calkujacy_Nyquist', 'rozniczkujacy_Nyquist', 'opozniajacy_Nyquist', 'op2_Nyquist'};
for i= 1:size(Models,2)
    figure('Name',char(titles(i)),'NumberTitle','off')
    [X, T] = step(Models(i));
    subplot(2,1,1);
    plot(T, X);
    xlabel('t');
    ylabel('x(t)');
    title('odpowiedz skokowa');
    [X, T] = impulse(Models(i));
    subplot(2,1,2);
    plot(T, X);
    xlabel('t');
    ylabel('x(t)');
    title('odpowiedz impulsowa');
    print(char(titles(i)),'-dpng')
end

for i= 1:size(Models,2)
    figure('Name',char(titles2(i)),'NumberTitle','off')
    bode(Models(i));
    title('bode');
    print(char(titles2(i)),'-dpng')
end

for i= 1:size(Models,2)
    figure('Name',char(titles3(i)),'NumberTitle','off')
    nyquist(Models(i));
    title('nyquist');
    print(char(titles3(i)),'-dpng')
end

%      a, b, c 
Ksi = [1,2,3;
       0, 1, 1 ];
for i = 1:size(Ksi,2)
    
end