clc;
clear all;
close all;
grid on;

s = tf('s');

% oscylacyjny
wn=2;
ksi=0.3;
Kosc=1;
Osc=tf(Kosc,[1 2*ksi*wn wn^2]);


titles = {'niestabilny','ksi = -1', 'niestabilny oscylacyjny' ,'na granicy stabilności', 'stabilny oscylacyjny','ksi = 1','stabilny'};
KSI = [-2, -1, -0.5, 0, 0.5, 1 , 2];
KA = 1;
WN = 1;
for i= 1:size(titles,2)
    Osc = tf(Kosc,[KA 2*KSI(i)*WN WN*WN]);
    figure('Name',char(titles(i)),'NumberTitle','off')
    [X, T] = step(Osc,30);
    subplot(3,1,1);
    plot(T, X);
    xlabel('t');
    ylabel('x(t)');
    title('odpowiedz skokowa');
    [X, T] = impulse(Osc,30);
    subplot(3,1,2);
    plot(T, X);
    xlabel('t');
    ylabel('x(t)');
    title('odpowiedz impulsowa');
    subplot(3,1,3);
    rlocus(Osc);
    print(char(titles(i)),'-dpng')
end

A = [0, 1, 1, 0 ,0, 1];
B = [-1, 0, 1, 1, 0, 0];
C = [1, 1, 0, 0, 2, 0]; 

titles = {'a = 0', 'b = 0' ,'c = 0', 'a i c = 0','a i b = 0','b i c =0' };
for i= 1:size(titles,2)
    drrz=tf(Kosc,[A(i) B(i) C(i)]);
    figure('Name',char(titles(i)),'NumberTitle','off')
    [X, T] = step(drrz,30);
    subplot(3,1,1);
    plot(T, X);
    xlabel('t');
    ylabel('x(t)');
    title('odpowiedz skokowa');
    [X, T] = impulse(drrz,30);
    subplot(3,1,2);
    plot(T, X);
    xlabel('t');
    ylabel('x(t)');
    title('odpowiedz impulsowa');
    subplot(3,1,3);
    rlocus(drrz);
    print(char(titles(i)),'-dpng')
end