+   
clc;
clear;
close;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    STALE     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
s = tf('s');
T1=[1:5];
T2=5;
k = 1;
w=0.25;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(1)
for i=1:5
    K = k/(T1(i)*s+1);
    hold on;
%     step(K)
%     impulse(K)
%     bode(K);
%     hold on;
%     nyquist(K);
%     hold on;
end
% figure(20)
% tau=0.5;
% st=10
% G1=tf(k,[T1 1]);
% [lo,mo] = pade(tau,st);
% G2=tf(lo,mo);
% G=series(G1,G2);
% hold on;
% bode(G)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(2)
for i=1:5
    K = k/((T1(i)*s+1)*(1+T2*s));
    hold on;
    step(K)
    impulse(K)
%     bode(K);
%     hold on;
%     nyquist(K);
%     hold on;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(3)
for i=1:5
    K = k/(T1(i)*T1(i)*s^2+2*w*T1(i)*s+1);
    hold on;
    step(K)
    impulse(K)
%     bode(K);
%     hold on;
%     nyquist(K);
%     hold on;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(4)
for i=1:5
    K = k/(s*(T1(i)*s+1));
    hold on;
    step(K)
    impulse(K)
%     bode(K);
%     hold on;
%     nyquist(K);
%     hold on;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(5)
for i=1:5
    K = k/(T1(i)*s+1);
    hold on;
    step(K)
    impulse(K)
%     bode(K);
%     hold on;
%     nyquist(K);
%     hold on;
end

figure(6)
for i=1:5
    K = k*exp(-T1(i)*s);
    hold on;
    step(K)
    impulse(K)
%     bode(K);
%     hold on;
%     nyquist(K);
%     hold on;
end
%%%%%%%%%%%%%%% STALE %%%%%%%%%%%%%%%%
